import {Component} from "@angular/core"
import {LoginService} from "../providers/login.service";
import {Router} from "@angular/router";

@Component({
  selector: "admin-side-menu",
  styles: [`
      nav {
          height: 0px;
      }
      .side-nav {
          background-color: #28397E;
          padding-top: 7rem;
      }
      .side-nav a{
        color: rgba(255, 255, 255, 0.72);
            font-size: larger;
    font-weight: 300;
    padding: 0px 50px;
      }
      .side-nav a i{
        color: rgba(255, 255, 255, 0.72);
        margin-right: 10px;
      }
      li.active a {
        color: white;
      }
      li.active a i {
        color: white;
      }
    `],
  templateUrl: "./admin-side-menu.component.html",
})
export class AdminSideMenuComponent {

  private menuList = [{display: "Content", path: "subjects", icon: "home"},
    {display: "Subscriptions", path: "subscriptions", icon: "toc"},
    {display: "Settings", path: "settings", icon: "settings"},
    {display: "Messaging", path: "campaign", icon: "message"},
    {display: "Analytics", path: "admin", icon: "timeline"},
    {display: "Users", path: "users", icon: "perm_identity"},
  ];

  constructor(public loginService:LoginService,
              public router:Router) {

  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['/login']);

  }
}
