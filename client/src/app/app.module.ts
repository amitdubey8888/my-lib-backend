import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AdminModule} from "./adminLogin/admin.module";
import {SuperAdminModule} from "./superAdmin/super-admin.module";
import {AppRoutingModule} from "./app-routing.module";
import {MaterializeModule} from "angular2-materialize/dist/index";
import {AngularFireModule} from 'angularfire2';
import {APP_BASE_HREF} from "@angular/common";
import {UploadImagesService} from './providers/upload-images.service';
import {Api} from "./providers/api";
import {CurrentUser} from "./providers/currentUser";
import {LoggedInGuard} from "./guards/loggedin.guard";
import {SuperuserGuard} from "./guards/superuser.guard";
import {LoginService} from "./providers/login.service";
import {AdminLoginComponent} from "./adminLogin/admin-login.component";
import {AppComponent} from './app.component';
import {Common} from './providers/common'

// Must export the config
export const firebaseConfig = {
  apiKey: "AIzaSyD7aUdLI1QjB_js7t8fBnSygHc2MecyIIQ",
  authDomain: "my-lib-project-46754.firebaseapp.com",
  databaseURL: "https://my-lib-project-46754.firebaseio.com",
  projectId: "my-lib-project-46754",
  storageBucket: "my-lib-project-46754.appspot.com",
  messagingSenderId: "515480162438"
};

@NgModule({
  declarations: [
    AppComponent,
    AdminLoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AdminModule,
    SuperAdminModule,
    AppRoutingModule,
    MaterializeModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'},
    Api,
    Common,
    CurrentUser,
    LoginService,
    UploadImagesService,
    LoggedInGuard,
    SuperuserGuard
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
