import {Component, EventEmitter, Inject} from "@angular/core";
import {MaterializeAction} from "angular2-materialize/dist/index";
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import * as firebase from 'firebase';
import {Api} from '../providers/api';
import {Common} from "../providers/common";
import * as Materialize from "angular2-materialize/dist/index";

@Component({
  selector: 'admin-subscription',
  styleUrls: ['./admin-subscription.component.css'],
  templateUrl: './admin-subscription.component.html',
})
export class AdminSubscriptionComponent {
  constructor(public api:Api,
              public common:Common,
              public af:AngularFire) {
    this.getSubscriptions();
  }

  public storageRef = firebase.storage().ref();
  public loading:boolean = false;
  public plans = [];
  public addPlan = {
    "subscription_title": "",
    "subscription_thumb": "",
    "subscription_amount": null,
    "subscription_duration": null
  };
  public editPlan = {
    "subscription_title": "",
    "subscription_thumb": "",
    "subscription_amount": null,
    "subscription_duration": null,
    "id":""
  };

  freeSubscription:boolean = false;

  addSubscription(form) {
    this.loading = true;
    this.api.post('subscriptions', this.addPlan)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          Materialize.toast(`${this.addPlan.subscription_title} added successfully`, 5000);
          this.getSubscriptions();
          this.closeModalAdd(form);
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        });
  }

  getSubscriptions():void {
    this.loading = true;
    this.api.get('subscriptions')
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          this.plans = response;
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
  }

  updateSubscription(form) {
    this.loading = true;
    this.api.patch('subscriptions/' + this.editPlan.id, this.editPlan)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          Materialize.toast(`${this.editPlan.subscription_title} updated successfully`, 5000);
          this.getSubscriptions();
          this.closeModalEdit(form);
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
  }

  deleteSubscription(plan):void {
    this.loading = true;
    this.api.delete('subscriptions/' + plan.id)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          Materialize.toast(`${plan.subscription_title} deleted successfully`, 5000);
          this.getSubscriptions();
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
  }

  subThumbUpload(event, edit) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'subscriptions/thumbs'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          this.loading = false;
          Materialize.toast(`Image uploaded successfully`, 5000);
          if (!edit)
            this.addPlan.subscription_thumb = snapshot.downloadURL;
          else
            this.editPlan.subscription_thumb = snapshot.downloadURL;
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }

  //_todo regex for only integers
  checkFreeAdd() {
    if (this.freeSubscription) {
      this.addPlan.subscription_amount = 0;
    }
  }

  checkFreeEdit() {
    if (this.freeSubscription) {
      this.editPlan.subscription_amount = 0;
    }
  }

  modalActionsAdd = new EventEmitter<string | MaterializeAction>();
  modalActionsEdit = new EventEmitter<string | MaterializeAction>();

  openModalAdd() {
    this.modalActionsAdd.emit({action: "modal", params: ['open']});
  }

  closeModalAdd(form) {
    this.modalActionsAdd.emit({action: "modal", params: ['close']});
    form.resetForm();
  }

  openModalEdit(plan) {
    this.editPlan = Object.assign({}, plan);
    this.modalActionsEdit.emit({action: "modal", params: ['open']});
  }

  closeModalEdit(form) {
    form.resetForm();
    this.modalActionsEdit.emit({action: "modal", params: ['close']});
  }
}
