import {Subscription} from "rxjs/Subscription";
import {Component, EventEmitter, OnDestroy, OnInit} from "@angular/core";
import {MaterializeAction} from "angular2-materialize/dist/index";
import {ActivatedRoute, Router} from "@angular/router";
import {Api} from '../providers/api';
import * as Materialize from "angular2-materialize/dist/index";

@Component({
  selector: 'book-topics',
  styleUrls: ['./book-topics.component.css'],
  templateUrl: "./book-topics.component.html",
})

export class BookTopicsComponent implements OnDestroy, OnInit {
  private bookId;
  private topics = [];
  public totalTerms = [1];
  public loading:boolean = false;
  private newTopic = {
    "topic_name": "",
    "topic_term": null,
    "bookId": ""
  };
  private editTopic = {
    "topic_name": "",
    "topic_term": null,
    "bookId": "",
    "id": ""
  };
  paramsSubscription:Subscription;

  constructor(private router:Router,
              private route:ActivatedRoute,
              private api:Api) {
  }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe(params => this.bookId = params['bookId']);
    this.getTopics();
    this.getTerms();
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  getTopics() {
    this.loading = true;
    this.api.get('topics?filter[where][bookId]=' + this.bookId)
      .map(res => res.json())
      .subscribe(
        response => {
          this.topics = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        });
  }

  onSelect(topic) {
    this.router.navigate(['../../topics', topic.id], {relativeTo: this.route});
  }

  addTopic(form):void {
    this.loading = true;
    this.newTopic.bookId = this.bookId;
    this.api.post('topics', this.newTopic)
      .map(res => res.json())
      .subscribe(
        newTopicCreated => {
          this.loading = false;
          Materialize.toast(`Successfully added ${this.newTopic.topic_name}`, 5000);
          this.getTopics();
          this.newTopic.topic_term = 1;
          this.closeModal(form);
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        });
  }

  updateTopic(form):void {
    this.loading = true;
    this.api.patch('topics/' + this.editTopic.id, this.editTopic)
      .map(res => res.json())
      .subscribe(
        response => {
          this.getTopics();
          Materialize.toast(`Successfully updated ${this.editTopic.topic_name}`, 5000);
          this.loading = false;
          this.closeModalEdit(form);
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        });
  }

  deleteTopic(topic):void {
    this.loading = true;
    this.api.delete('topics/' + topic.id)
      .map(res => res.json())
      .subscribe(
        response => {
          this.getTopics();
          this.loading = false;
          Materialize.toast(`Successfully deleted ${topic.topic_name}`, 5000);
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        }
      );
  }

  getTerms() {
    this.loading = true;
    this.api.get(`books/?filter[where][id]=${this.bookId}`)
      .map(res => res.json())
      .subscribe(
        response => {
          this.totalTerms = Array(response[0].termsCount).fill(1);
          this.newTopic.topic_term = 1;
          this.loading = false;
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        }
      );
  }

  modalActions = new EventEmitter<string|MaterializeAction>();
  modalActionsEdit = new EventEmitter<string|MaterializeAction>();

  openModal() {
    this.modalActions.emit({action: "modal", params: ['open']});
  }

  closeModal(form) {
    form.resetForm();
    this.modalActions.emit({action: "modal", params: ['close']});
  }

  openModalEdit(topic) {
    this.editTopic = Object.assign({}, topic);
    this.modalActionsEdit.emit({action: "modal", params: ['open']});
  }

  closeModalEdit(form) {
    form.resetForm();
    this.modalActionsEdit.emit({action: "modal", params: ['close']});
  }
}
