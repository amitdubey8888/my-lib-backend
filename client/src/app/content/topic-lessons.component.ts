import {Subscription} from "rxjs/Subscription";
import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Api} from '../providers/api';

@Component({
  selector: 'topic-lessons',
  styleUrls: ["./topic-lessons.component.css"],
  templateUrl: "./topic-lessons.component.html",
})

export class TopicLessonsComponent implements OnDestroy, OnInit {
  private topicId;
  private lessons = [];
  paramsSubscription:Subscription;
  private loading:boolean = false;

  constructor(private router:Router,
              private route:ActivatedRoute,
              private api:Api) {
  }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe(params => this.topicId = params['topicId']);
    this.getLessons();
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  addNewLesson() {
    this.router.navigate(['../../lesson', this.topicId], {relativeTo: this.route});
  }

  editOldLesson(lesson) {
    this.router.navigate(['../../lessons', lesson.id], {relativeTo: this.route});
  }

  getLessons() {
    this.loading = true;
    this.api.get('lessons?filter[where][topicId]=' + this.topicId)
      .map(res => res.json())
      .subscribe(
        response => {
          this.lessons = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  deleteLesson(lesson):void {
    this.loading = true;
    this.api.delete('lessons/' + lesson.id)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          this.getLessons()
        },
        error => {
          this.loading = false;
        }
      )
  }
}
