import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AdminLoginComponent} from "./adminLogin/admin-login.component";
import {SuperAdminMenuComponent} from "./superAdmin/super-admin-menu.component";

const appRoutes:Routes = [
  {path: 'login',component:AdminLoginComponent},
  {path: '',   redirectTo: '/login', pathMatch: 'full' },
  {path: '**', redirectTo: '/login' }
];
@NgModule({
  imports:[
    RouterModule.forRoot(appRoutes,{useHash:true})
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule {

}
