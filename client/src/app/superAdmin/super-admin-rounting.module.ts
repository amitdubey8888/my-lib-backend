import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {SuperAdminMenuComponent} from "./super-admin-menu.component";
import {SuperAdminHomeComponent} from "./super-admin-home.component";
import {SuperAdminSettingComponent} from "./super-admin-setting.component";
import {SuperAdminLoginComponent} from "./super-admin-login.component";
import {SuperuserGuard} from "../guards/superuser.guard";
const superAdminRoutes:Routes = [
  {
    path: 'super', component: SuperAdminMenuComponent,
    children: [
      {path: "home", component: SuperAdminHomeComponent, canActivate: [SuperuserGuard],},
      {path: "settings", component: SuperAdminSettingComponent, canActivate: [SuperuserGuard],},
      /*{path: '',   redirectTo: '/home', pathMatch: 'full' }*/
    ]
  },
  {path: 'super_login', component: SuperAdminLoginComponent}
];
@NgModule({
  imports: [RouterModule.forChild(superAdminRoutes)],
  exports: [RouterModule]
})
export class SuperAdminRouting {
}
