import {Component} from "@angular/core"
import {LoginService} from "../providers/login.service";
import {Router} from "@angular/router";
@Component({
  selector: "super-admin-menu",
  styles: [`
      nav {
          height: 0px;
      }
      .side-nav {
          background-color: #28397E;
          padding-top: 13rem;
          vertical-align: middle;
      }
      .side-nav a{
        color: rgba(255, 255, 255, 0.72);
            font-size: larger;
    font-weight: 300;
    padding: 0px 50px;
      }
      .side-nav a i{
        color: rgba(255, 255, 255, 0.72);
        margin-right: 10px;
      }
      li.active a {
        color: white;
      }
      li.active a i {
        color: white;
      }
   
    `],
  templateUrl: "./super-admin-menu.component.html"
})
export class SuperAdminMenuComponent {

  private menuList = [
    {display: "Administrators", path: "home", icon: "home"},
    {display: "Settings", path: "settings", icon: "settings"}
  ];

  constructor(public loginService: LoginService,public router:Router){

  }
  logout() {
    this.loginService.logout();
    this.router.navigate(['/super_login']);

  }
}
