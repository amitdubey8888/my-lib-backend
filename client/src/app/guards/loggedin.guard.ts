import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {CurrentUser} from "../providers/currentUser";
import {Injectable} from "@angular/core";

@Injectable()
export class LoggedInGuard implements CanActivate {

  constructor(public currentUser:CurrentUser,
              public router:Router) {

  }

  canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot) {
    if (!this.currentUser.info) {
      let userToken = localStorage.getItem('currentUserToken') || null;
      if (userToken)
        return true;
      else {
        this.router.navigate(['/login']);
        return false;
      }
    }
    else return this.currentUser.info.realm == 'admin';
  }
}
