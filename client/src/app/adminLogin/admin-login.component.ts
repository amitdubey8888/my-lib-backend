import {Component, OnInit, EventEmitter} from "@angular/core";
import {Router, ActivatedRoute} from '@angular/router';
import {NgForm} from "@angular/forms";
import {LoginService} from "../providers/login.service"
import {MaterializeAction} from "angular2-materialize/dist/index";
import * as Materialize from "angular2-materialize/dist/index";
import {CurrentUser} from "../providers/currentUser";

@Component({
  selector: 'admin-login',
  templateUrl: "./admin-login.component.html",
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  username = 'admin1';
  password = 'admin1';
  loading = false;
  returnUrl:string;
  resetEmail = '';

  constructor(private route:ActivatedRoute,
              private router:Router,
              private currentUser:CurrentUser,
              private loginService:LoginService,) {
  }

  onSubmit(form:NgForm) {
    this.loading = true;
    this.loginService.customLogin(this.username, this.password, 'admin')
      .subscribe(
        data => {
          this.loading = false;
          this.router.navigate([this.returnUrl]);
          Materialize.toast(`Logged in as ${this.username}`, 5000);
          form.resetForm();
        },
        error => {
          // console.warn("error");
          Materialize.toast(`Incorrect Username or password`, 5000);
          this.loading = false;
        });
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/admin';
    if (!this.currentUser.info) {
      let userToken = localStorage.getItem('currentUserToken') || null;
      userToken = JSON.parse(userToken);
      if (userToken) {
        this.loading = true;
        this.loginService.getDetailsViaToken(userToken)
          .subscribe(
            user => {
              this.loading = false;
              this.currentUser.info = user.json();
              this.router.navigate([this.returnUrl]);
            },
            error => {
              this.loading = false;
              Materialize.toast(`Unable to connect, Please check your connection`, Infinity)
            }
          );
      }
      else {
        this.currentUser.info = null;
      }
    }
    else if (this.currentUser.info.realm == 'admin') {
      this.router.navigate([this.returnUrl]);
    }
    else {
      this.currentUser.info = null;
    }
  }

  modalActions = new EventEmitter<string|MaterializeAction>();

  openModal() {
    this.modalActions.emit({action: "modal", params: ['open']})
  }

  closeModal() {
    this.modalActions.emit({action: "modal", params: ['close']})
  }

  send(form) {
    this.loading = true;
    this.loginService.resetPassword(this.resetEmail)
      .map((response:Response) => response.json())
      .subscribe(res => {
        this.loading = false;
        Materialize.toast(`Reset instructions sent to ${this.resetEmail}`, 5000);
        this.closeModal();
        form.resetForm();
      }, err => {
        //_handle errors more comprehensively
        this.loading = false;
        // console.error(err);
        Materialize.toast(`Unable to find ${this.resetEmail}! Are you registered?`, 5000);
      });
  }
}
