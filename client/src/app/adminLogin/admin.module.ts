import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {AdminSideMenuComponent} from "../sideMenu/admin-side-menu.component";
import {AdminLoginComponent} from "./admin-login.component";
import {AdminUsersComponent} from "../users/admin-users.component";
import {AdminSubjectsComponent} from "../content/admin-subjects.component";
import {AdminSubscriptionComponent} from "../subscription/admin-subscription.component";
import {AdminSettingsComponent} from "../settings/admin-settings.component";
import {AdminCampaignComponent} from "../messaging/admin-campaign.component";
import {SubjectBooksComponent} from "../content/subject-books.component";
import {BookTopicsComponent} from "../content/book-topics.component";
import {TopicLessonsComponent} from "../content/topic-lessons.component";
import {LessonSectionsComponent} from "../content/lesson-sections.component";
import {AddSectionsComponent} from "../content/add-sections.component";
import {AdminRoutingModule} from "./admin-routing.module";
import {MaterializeModule} from "angular2-materialize/dist/index";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AdminRoutingModule,
    MaterializeModule
  ],
  declarations: [
    AdminSideMenuComponent,
    AdminUsersComponent,
    AdminSubjectsComponent,
    AdminSubscriptionComponent,
    AdminSettingsComponent,
    AdminCampaignComponent,
    SubjectBooksComponent,
    BookTopicsComponent,
    TopicLessonsComponent,
    LessonSectionsComponent,
    AddSectionsComponent,
  ]
})
export class AdminModule {
}
