import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {Injectable} from '@angular/core'
import {Http, Headers, Response} from '@angular/http';
import {Api} from './api';
import {CurrentUser} from './currentUser';

@Injectable()
export class LoginService {
  constructor(private http:Http,
              private api:Api,
              public currentUser:CurrentUser) {
  }

  customLogin(username:string, password:string, realm:string):Observable<any> {
    return this.api.post('users/login', {username: username, password: password, realm: realm})
      .map((response:Response) => {
        // login successful if there's a jwt token in the response
        let token = response.json();
        if (token && token.userId) {
          // store token details and jwt token in local storage to keep user logged in between page refreshes
          this.getDetailsViaToken(token)
            .subscribe((sub) => {
              this.currentUser.info = sub.json();
            });
          if (realm == 'superuser')
            localStorage.setItem('superUserToken', JSON.stringify(token));
          else
            localStorage.setItem('currentUserToken', JSON.stringify(token));
        }
      });
  }

  resetPassword(email:string):Observable<any> {
    return this.api.post('users/reset', {"email": email})
  }

  logout() {
    // remove user from local storage to log user out
    console.info("Logged Out");
    localStorage.removeItem('currentUserToken');
    localStorage.removeItem('superUserToken');
    this.currentUser.info = null;
  }

  getDetailsViaToken(token) {
    return this.api.get('users/' + token.userId, {access_token: token.id})
  }
}
