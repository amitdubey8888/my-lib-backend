import {Injectable} from '@angular/core';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import * as firebase from 'firebase';
import * as _ from 'lodash';

@Injectable()
export class UploadImagesService {

  public IMAGES_FOLDER: string = "images";
  constructor(public af:AngularFire) {
  }

  public uploadImagesToFirebase(files, folderName): any {
    let storageRef = firebase.storage().ref();
    _.each(files, (item) => {

      item.isUploading = true;
      let uploadTask:firebase.storage.UploadTask = storageRef.child(`${this.IMAGES_FOLDER}/${item.name}`).put(item);

     return uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot) => item.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
        (error) => {
        },
        () => {
          item.url = uploadTask.snapshot.downloadURL;
          item.isUploading = false;
          console.info("link", item.url);
          return item.url;
          // this.saveImage({name: item.file.name, url: item.url}, IMAGES_FOLDER);
        }
      );

    });
  }

  private saveImage(image:any) {
    this.af.database.list(`/${this.IMAGES_FOLDER}`).push(image);
  }
}
